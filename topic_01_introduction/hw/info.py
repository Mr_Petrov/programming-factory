"""
Ввод данных с клавиатуры:
- Имя
- Фамилия
- Возраст

Вывод данных в формате:
"Вас зовут <Имя> <Фамилия>! Ваш возраст равен <Возраст>."
"""

user_name = input('What is your name? ')
user_name1 = input('What is your last name?')
user_name2 = input('How old are you? ')
"""
print("Your name is", user_name, user_name1, end="")
print("!", end="")
print(" You are", user_name2, "years old", end="")
print(".")
"""
print("Your name is ", user_name, " ", user_name1, "!", " You are ", user_name2, " ", "years old.",
      sep="")


