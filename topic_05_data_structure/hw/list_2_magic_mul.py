"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def magic_mul(my_list):
    if type(my_list) != list:
        return 'Must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'

    result = []
    my_list_first_el = my_list[:1]
    my_list_mul3 = my_list * 3
    my_list_ext = my_list[-1]

    result.extend(my_list_first_el)
    result.extend(my_list_mul3)
    result.append(my_list_ext)

    return result


if __name__ == '__main__':
    magic_mul([5,6,7])
    print(magic_mul([5,6,7]))
