"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку  'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""

def zip_names(el1, el2):
    if type(el1) != list:
        return 'First arg must be list!'
    if type(el2) != set:
        return 'Second arg must be set!'
    if len(el1) == 0:
        return 'Empty list!'
    if len(el2) == 0:
        return 'Empty set!'
    return list(zip(el1, el2))
