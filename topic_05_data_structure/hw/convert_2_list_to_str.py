"""
Функция list_to_str.

Принимает 2 аргумента: список и разделитель (строка).

Возвращает (строку полученную разделением элементов списка разделителем,
количество разделителей в получившейся строке в квадрате).

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""


def list_to_str(my_list, my_sep):
    if type(my_list) != list:
        return 'First arg must be list!'
    if type(my_sep) != str:
        return 'Second arg must be str!'
    if not my_list:
        return ()
    string1 = my_sep.join([str(el) for el in my_list])
    count_sep = string1.count(my_sep)
    return string1, count_sep ** 2
