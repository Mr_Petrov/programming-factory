"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    if not my_dict:
        return ([], [], 0, 0)
    keys = list(my_dict.keys())
    values = list(my_dict.values())
    unique_keys = len(set(keys))
    unique_values = len(set(values))
    return keys, values, unique_keys, unique_values


