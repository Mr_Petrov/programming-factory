"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(my_dict, word):
    if type(my_dict) != dict:
        return "Dictionary must be dict!"
    elif type(word) != str:
        return "Word must be str!"
    elif len(my_dict) == 0:
        return "Dictionary is empty!"
    elif len(word) == 0:
        return "Word is empty!"
    elif word in my_dict.values():
        return word
    else:
        rus_translations = []
        for rus, en in my_dict.items():
            if word in en:
                rus_translations.append(rus)
        return rus_translations if len(rus_translations) > 0 else f"Can't find English word: {word}"

