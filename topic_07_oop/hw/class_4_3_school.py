from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker

"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""


class School:
    def __init__(self, people, number):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        marks_list = [x.get_avg_mark() for x in self.people if isinstance(x, Pupil)]
        return sum(marks_list) / len(marks_list)

    def get_avg_salary(self):
        all_zp = [y.salary for y in self.people if isinstance(y, Worker)]
        return sum(all_zp) / len(all_zp)

    def get_worker_count(self):
        return sum(1 for worker in self.people if type(worker) == Worker)

    def get_pupil_count(self):
        return sum(1 for pupil in self.people if type(pupil) == Pupil)

    def get_pupil_names(self):
        return [n.name for n in self.people if isinstance(n, Pupil)]

    def get_unique_worker_positions(self):
        return {p.position for p in self.people if isinstance(p, Worker)}

    def get_max_pupil_age(self):
        return max([a.age for a in self.people if isinstance(a, Pupil)])

    def get_min_worker_salary(self):
        return min([s.salary for s in self.people if isinstance(s, Worker)])

    def get_min_salary_worker_names(self):

        return [l.name for l in self.people if isinstance(l, Worker) if l.salary == self.get_min_worker_salary()]




