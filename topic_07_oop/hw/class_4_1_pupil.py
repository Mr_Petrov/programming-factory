"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""


class Pupil:
    def __init__(self, name, age, marks: dict):
        self.name = name
        self.age = age
        self.marks = marks

    def get_all_marks(self):
        marks = []
        for x in self.marks.values():
            marks.extend(x)
        return marks

    def get_avg_mark_by_subject(self, subject):
        marks_sum = []
        for sub, mar in self.marks.items():
            if subject in sub:
                marks_sum.extend(mar)
        return sum(marks_sum) / len(marks_sum) if len(marks_sum) > 0 else 0

    def get_avg_mark(self):
        avg_all_sub = []
        for m in self.marks.values():
            avg_all_sub.extend(m)
        return sum(avg_all_sub) / len(avg_all_sub)

    def __le__(self, pl2):
        val1 = []
        val2 = []
        for mar in self.marks.values():
            val1.extend(mar)
            for mar2 in pl2.marks.values():
                val2.extend(mar2)
        return sum(val1)/len(val1) <= sum(val2)/len(val2)
