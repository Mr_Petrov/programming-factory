from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat


"""
Класс Farm.

Поля:
животные (list из произвольного количества Goat и Chicken): zoo_animals
(вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
наименование фермы: name,
имя владельца фермы: owner.

Методы:
get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
get_chicken_count: вернуть количество куриц на ферме,
get_animals_count: вернуть количество животных на ферме,
get_milk_count: вернуть сколько молока можно получить в день,
get_eggs_count: вернуть сколько яиц можно получить в день.
"""


class Farm:
    def __init__(self, name, owner):
        self.zoo_animals = []
        self.name = name
        self.owner = owner

    def add_one_chicks(self, chicken: Chicken):
        self.zoo_animals.append(chicken)

    def add_some_goats(self, goats: Goat):
        self.zoo_animals.extend(goats)

    def get_goat_count(self):
        return len([x for x in self.zoo_animals if isinstance(x, Goat)])
        # return self.zoo_animals.count(type(Goat))

    def get_chicken_count(self):
        return len([y for y in self.zoo_animals if isinstance(y, Chicken)])
        # return self.zoo_animals.count(type(Chicken))

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        return sum(n.milk_per_day for n in self.zoo_animals if isinstance(n, Goat))

    def get_eggs_count(self):
        return sum(m.eggs_per_day for m in self.zoo_animals if isinstance(m, Chicken))
