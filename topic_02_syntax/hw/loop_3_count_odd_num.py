"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
Если число меньше или равно 0, то вернуть "Must be > 0!".
"""
def count_odd_num(n):
    if type(n) != int:
        return "Must be int!"
    if n <= 0:
        return "Must be > 0!"
    acc = 0
    while n > 0:
        if n % 2 != 0:
            acc += 1
        n = n // 10
    return acc
if __name__ == '__main__':
    result = count_odd_num(1234567)
    print(result)





