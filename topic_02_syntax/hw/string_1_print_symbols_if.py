"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(string):
    type_change = str(string)
    length = len(type_change)
    if length == 0:
        print("Empty string!")
    elif length > 5:
        print(type_change[0:3] + type_change[-3:])
    else:
        print(type_change[0] * length)


if __name__ == '__main__':
    print_symbols_if(44455777)

