"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""
def sum_1_112_3():
    acc = 0
    for i in range(1, 113, 3):
        acc = acc + i
    return acc
if __name__ == '__main__':
    result = sum_1_112_3()
    print(result)
