"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(string1, string2):
    diff = len(string1) - len(string2)
    if diff == 0:
        return False

    if len(string1) == 0 or len(string2) == 0:
        return True
    if string1 in string2 or string2 in string1:
        return True
    else:
        return False


if __name__ == '__main__':
    check_substr("Ghbdtnjh ", "kjh")
