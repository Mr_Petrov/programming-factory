"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(path, my_dict):
    with open(path, "w") as f:
        f.write(str(my_dict))
    with open(path, "r") as fl:
        print(f.read())


if __name__ == "__main__":
    save_dict_to_file_classic("my_dict.txt", {1: "a", 2: "b", 3: "c"})
#     with open("my_dict.txt", "r") as fl:
#         print(fl.read())

