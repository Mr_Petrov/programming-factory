"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(path, my_dict):
    with open(path, "wb") as pick:
        pickle.dump(my_dict, pick)


if __name__ == "__main__":
    d = {1: "qwe", 2: ["nfwijnfi"], 3: {23445}}
    save_dict_to_file_pickle("new_file", d)
    with open("new_file", "rb") as f:
        load = pickle.load(f)

    print(f'type: {type(load)}')
    print(f'equal: {d == load}')
